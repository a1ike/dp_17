$(document).ready(function () {

  $('#d-fullpage').fullpage({
    //Navigation
    //menu: '#myMenu',
    lockAnchors: false,
    anchors: ['pageOne', 'pageTwo', 'pageThree', 'pageFour', 'pageFive', 'pageSix', 'pageSeven', 'pageEight'],
    navigation: true,
    navigationPosition: 'right',
    navigationTooltips: ['Главная', 'Что такое перфоманс', 'Этапы', 'Отзывы', 'Команда', 'Цены', 'Клиенты', 'Контакты'],
    showActiveTooltip: false,
    slidesNavigation: true,
    slidesNavPosition: 'right',

    //Scrolling
    css3: true,
    scrollingSpeed: 400,
    autoScrolling: true,
    fitToSection: true,
    fitToSectionDelay: 1000,
    scrollBar: true,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    loopBottom: false,
    loopTop: false,
    loopHorizontal: true,
    continuousVertical: false,
    continuousHorizontal: false,
    scrollHorizontally: false,
    interlockedSlides: false,
    dragAndMove: false,
    offsetSections: false,
    resetSliders: false,
    fadingEffect: false,
    //normalScrollElements: '#element1, .element2',
    scrollOverflow: false,
    touchSensitivity: 15,
    normalScrollElementTouchThreshold: 5,
    bigSectionsDestination: null,

    //Accessibility
    keyboardScrolling: true,
    animateAnchor: true,
    recordHistory: true,

    //Design
    controlArrows: true,
    verticalCentered: true,
    sectionsColor: ['#fff'],
    paddingTop: '0px',
    paddingBottom: '0px',
    //fixedElements: '#myMenu, .footer',
    responsiveWidth: 1200,
    responsiveHeight: 0,
    responsiveSlides: false,
    parallax: true,
    parallaxKey: 'YWx2YXJvdHJpZ28uY29tXzlNZGNHRnlZV3hzWVhnPTFyRQ==',
    parallaxOptions: {
      type: 'reveal',
      percentage: 62,
      property: 'translate'
    },
    //Custom selectors
    sectionSelector: '.section',
    slideSelector: '.slide',

    lazyLoading: true,

    //events
    onLeave: function (index, nextIndex, direction) { },
    afterLoad: function (anchorLink, index) {
    },
    afterRender: function () {
      new WOW().init();
    },
    afterResize: function () { },
    afterResponsive: function (isResponsive) { },
    afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
    },
    onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) { }
  });

  $('.d-result__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.d-team__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.d-companies__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.d-nav__toggle').click(function () {
    $('.d-menu').slideToggle('fast', function () {
    });
  });

  $('.d-menu').click(function (e) {
    if (e.target.id == 'd-menu') {
      $('.d-menu').slideToggle('fast', function () {
      });
    }
  });

});